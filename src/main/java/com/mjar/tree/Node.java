package com.mjar.tree;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Node<T extends Comparable<T>> implements Comparable<Node<T>> {

    private final T id;

    private final int level;

    private final Map<T, Node<T>> children;

    public Node(final T id, final int level) {
        this.id = id;
        this.level = level;
        children = new TreeMap<>();
    }

    public T getId() {

        return id;
    }

    public int getLevel() {

        return level;
    }

    public Node<T> withChild(final Node<T> child) {

        addChild(child);
        return this;
    }

    public Node<T> withChildren(final Collection<Node<T>> children) {

        addChildren(children);
        return this;
    }

    public void addChild(final Node<T> child) {

        children.put(child.id, child);
    }

    public void addChildren(final Collection<Node<T>> children) {

        for (final Node<T> child : children) {
            addChild(child);
        }
    }

    public Collection<Node<T>> getChildren() {

        return Collections.unmodifiableCollection(children.values());
    }

    public void removeChild(final Node<T> child) {

        children.remove(child.id);
    }

    public boolean hasChildren() {

        return !children.isEmpty();
    }

    public boolean hasChildren(final Collection<Node<T>> children) {

        for (final Node<T> thatChild : children) {
            final Node<T> thisChild = this.children.get(thatChild.id);
            if (null == thisChild || !thisChild.equals(thatChild)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {

        final StringBuilder result = new StringBuilder("Node(").append(id).append(")");
        if (hasChildren()) {
            result.append(children.values().toString());
        }
        return result.toString();
    }

    @Override
    public int compareTo(final Node<T> that) {

        return this.id.compareTo(that.id);
    }

    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Node<T> that = (Node<T>) obj;
        if (!this.id.equals(that.id)) {
            return false;
        }
        if (this.children.size() != that.children.size()) {
            return false;
        }
        return hasChildren(that.children.values());
    }

    @Override
    public int hashCode() {

        int result = id.hashCode();
        result = 37 * result + children.hashCode();
        return result;
    }
}
