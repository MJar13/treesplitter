package com.mjar.tree;

import java.util.*;

public class TreeSplitter {

    public <T extends Comparable<T>> Collection<Node<T>> split(final Collection<Node<T>> sourceStructure,
            final Collection<Node<T>> maskStructure) {

        if (null == maskStructure || maskStructure.isEmpty() || null == sourceStructure || sourceStructure.isEmpty()) {
            if (null == sourceStructure) {
                return Collections.emptyList();
            }
            return sourceStructure;
        }
        final Collection<Leaf<T>> leaves = collectLeaves(sourceStructure, maskStructure);
        return split(leaves);
    }

    private <T extends Comparable<T>> Collection<Leaf<T>> collectLeaves(final Collection<Node<T>> sourceStructure,
            final Collection<Node<T>> maskStructure) {

        final Map<T, Leaf<T>> leaves = new HashMap<>();
        // FIXME: remove code duplication, extract collector class?
        addSourceLeaves(leaves, sourceStructure, Collections.<Node<T>> emptySet());
        addMaskLeaves(leaves, maskStructure, Collections.<Node<T>> emptySet());
        return leaves.values();
    }

    private <T extends Comparable<T>> void addSourceLeaves(final Map<T, Leaf<T>> leaves,
            final Collection<Node<T>> sourceStructure,
            final Collection<Node<T>> parents) {

        for (final Node<T> node : sourceStructure) {
            if (node.hasChildren()) {
                final Collection<Node<T>> nodeParents = new HashSet<>(parents);
                nodeParents.add(node);
                addSourceLeaves(leaves, node.getChildren(), nodeParents);
            } else {
                final T leafId = node.getId();
                Leaf<T> leaf = leaves.get(leafId);
                if (null == leaf) {
                    leaf = new Leaf<>(node);
                    leaves.put(leafId, leaf);
                }
                for (final Node<T> parentNode : parents) {
                    leaf.addSourceParent(parentNode);
                }
            }
        }
    }

    private <T extends Comparable<T>> void addMaskLeaves(final Map<T, Leaf<T>> leaves,
            final Collection<Node<T>> maskStructure,
            final Collection<Node<T>> parents) {

        for (final Node<T> node : maskStructure) {
            if (node.hasChildren()) {
                final Collection<Node<T>> nodeParents = new HashSet<>(parents);
                nodeParents.add(node);
                addMaskLeaves(leaves, node.getChildren(), nodeParents);
            } else {
                final T leafId = node.getId();
                Leaf<T> leaf = leaves.get(leafId);
                if (null == leaf) {
                    leaf = new Leaf<>(node);
                    leaves.put(leafId, leaf);
                }
                for (final Node<T> parentNode : parents) {
                    leaf.addMaskParent(parentNode);
                }
            }
        }
    }

    private <T extends Comparable<T>> Collection<Node<T>> split(final Collection<Leaf<T>> leaves) {

        final Collection<Node<T>> roots = new TreeSet<>();
        final Map<T, Node<T>> allNodes = new HashMap<>();
        final Map<T, T> possibleSourceReplacements = new HashMap<>();
        final Map<T, Node<T>> possibleSourceReplacementParents = new HashMap<>();
        for (final Leaf<T> leaf : leaves) {
            final Map<Integer, Node<T>> sourceParents = leaf.getSourceParentsByLevel();
            final Map<Integer, Node<T>> maskParents = leaf.getMaskParentsByLevel();
            Node<T> parent = null;
            for (int i = 0; i < 20; i++) {
                Node<T> currentLeafParent = null;
                if (maskParents.isEmpty()) {
                    currentLeafParent = sourceParents.get(i);
                } else {
                    currentLeafParent = maskParents.get(i);
                    if (currentLeafParent != null && sourceParents.containsKey(i)) {
                        possibleSourceReplacements.put(currentLeafParent.getId(), sourceParents.get(i).getId());
                        possibleSourceReplacementParents.put(currentLeafParent.getId(), parent);
                    }
                }
                if (null == currentLeafParent) {
                    continue;
                }
                final T id = currentLeafParent.getId();
                Node<T> node = allNodes.get(id);
                if (null == node) {
                    node = new Node<>(id, i);
                    allNodes.put(id, node);
                    if (null == parent) {
                        roots.add(node);
                    }
                }
                if (null != parent) {
                    parent.addChild(node);
                }
                parent = node;
            }
            if (null != parent) {
                final Node<T> leafNode = new Node<>(leaf.getLeafNode().getId(), 0);
                parent.addChild(leafNode);
            }
        }
        for (final Map.Entry<T, T> posibleReplacement : possibleSourceReplacements.entrySet()) {
            if (!allNodes.containsKey(posibleReplacement.getValue())) {
                final Node<T> parent = possibleSourceReplacementParents.get(posibleReplacement.getKey());
                final Node<T> node = allNodes.get(posibleReplacement.getKey());
                parent.removeChild(node);
                final Node<T> newNode = new Node<>(posibleReplacement.getValue(), node.getLevel()).withChildren(node.getChildren());
                parent.addChild(newNode);
            }
        }
        return roots;
    }
}