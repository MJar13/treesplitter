package com.mjar.tree;

import java.util.HashMap;
import java.util.Map;

public class Leaf<T extends Comparable<T>> implements Comparable<Leaf<T>> {

    private final Node<T> leafNode;

    private final Map<Integer, Node<T>> sourceParentsByLevel;

    private final Map<Integer, Node<T>> maskParentsByLevel;

    public Leaf(final Node<T> leafNode) {
        super();
        this.leafNode = leafNode;
        this.sourceParentsByLevel = new HashMap<>();
        this.maskParentsByLevel = new HashMap<>();
    }

    public void addSourceParent(final Node<T> parent) {

        sourceParentsByLevel.put(parent.getLevel(), parent);
    }

    public void addMaskParent(final Node<T> parent) {

        maskParentsByLevel.put(parent.getLevel(), parent);
    }

    public Node<T> getLeafNode() {

        return leafNode;
    }

    public Map<Integer, Node<T>> getSourceParentsByLevel() {

        return sourceParentsByLevel;
    }

    public Map<Integer, Node<T>> getMaskParentsByLevel() {

        return maskParentsByLevel;
    }

    @Override
    public int compareTo(final Leaf<T> o) {

        return this.leafNode.compareTo(o.leafNode);
    }
}
