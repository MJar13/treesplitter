package com.mjar.tree;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Test;

public class TreeSplitterTest {

    @Test
    public void shouldSplitEmptyInput() {

        // Given
        final TreeSplitter splitter = new TreeSplitter();
        // When
        // Then
        assertTrue(splitter.split(null, null).isEmpty());
        assertTrue(splitter.split(null, Collections.<Node<Integer>> emptyList()).isEmpty());
        assertTrue(splitter.split(null, Collections.singleton(new Node<>(1, 0))).isEmpty());
        assertTrue(splitter.split(Collections.<Node<Integer>> emptyList(), Collections.singleton(new Node<>(1, 0))).isEmpty());
    }

    @Test
    public void shouldPreserveAdminAreaStructureWhenNoGeopoliticalRegions() {

        // Given
        final TreeSplitter splitter = new TreeSplitter();
        final Node<Integer> adminAreaTree = new Node<>(1, 0);
        // When
        final Collection<Node<Integer>> splited =
            splitter.split(Collections.singleton(adminAreaTree), Collections.<Node<Integer>> emptyList());
        // Then
        assertThat(splited.size(), is(1));
        assertThat(splited.iterator().next(), is(adminAreaTree));
    }

    @Test
    public void shouldMoveNodeToGeopoliticalCountry() {

        // Given
        final TreeSplitter splitter = new TreeSplitter();
        final Node<Integer> adminAreaTree = new Node<>(1, 0)
                .withChild(new Node<>(11, 1))
                .withChild(new Node<>(12, 1));
        final Node<Integer> geopoliticalTree = new Node<>(2, 0)
                .withChild(new Node<>(12, 1));
        // When
        final Collection<Node<Integer>> splited =
            splitter.split(Collections.singleton(adminAreaTree), Collections.singleton(geopoliticalTree));
        // Then
        final Node<Integer> actualTrees = new Node<>(0, 0).withChildren(splited);
        final Node<Integer> expectedTrees = new Node<>(0, 0)
                .withChild(new Node<>(1, 0)
                        .withChild(new Node<>(11, 1)))
                .withChild(new Node<>(2, 0)
                        .withChild(new Node<>(12, 1)));
        assertThat("Actual and expected trees are different!", actualTrees, is(expectedTrees));
    }

    @Test
    public void shouldSplitNode() {

        // Given
        final TreeSplitter splitter = new TreeSplitter();
        final Node<Integer> adminAreaTree = new Node<>(1, 0)
                .withChild(new Node<>(11, 1)
                        .withChild(new Node<>(111, 2))
                        .withChild(new Node<>(112, 2)));
        final Node<Integer> geopoliticalTree = new Node<>(2, 0)
                .withChild(new Node<>(21, 1)
                        .withChild(new Node<>(112, 2)));
        // When
        final Collection<Node<Integer>> splited =
            splitter.split(Collections.singleton(adminAreaTree), Collections.singleton(geopoliticalTree));
        // Then
        final Node<Integer> actualTrees = new Node<>(0, 0).withChildren(splited);
        final Node<Integer> expectedTrees = new Node<>(0, 0)
                .withChild(new Node<>(1, 0)
                        .withChild(new Node<>(11, 1)
                                .withChild(new Node<>(111, 2))))
                .withChild(new Node<>(2, 0)
                        .withChild(new Node<>(21, 1)
                                .withChild(new Node<>(112, 2))));
        assertThat("Actual and expected trees are different!", actualTrees, is(expectedTrees));
    }

    @Test
    public void shouldMoveNodeWithChildren() {

        // Given
        final TreeSplitter splitter = new TreeSplitter();
        final Node<Integer> adminAreaTree = new Node<>(1, 0)
                .withChild(new Node<>(11, 1)
                        .withChild(new Node<>(111, 2)))
                .withChild(new Node<>(12, 1)
                        .withChild(new Node<>(121, 2)));
        final Node<Integer> geopoliticalTree = new Node<>(2, 0)
                .withChild(new Node<>(21, 1)
                        .withChild(new Node<>(121, 2)));
        // When
        final Collection<Node<Integer>> splited =
            splitter.split(Collections.singleton(adminAreaTree), Collections.singleton(geopoliticalTree));
        // Then
        final Node<Integer> actualTrees = new Node<>(0, 0).withChildren(splited);
        final Node<Integer> expectedTrees = new Node<>(0, 0)
                .withChild(new Node<>(1, 0)
                        .withChild(new Node<>(11, 1)
                                .withChild(new Node<>(111, 2))))
                .withChild(new Node<>(2, 0)
                        .withChild(new Node<>(12, 1)
                                .withChild(new Node<>(121, 2))));
        assertThat("Actual and expected trees are different!", actualTrees, is(expectedTrees));
    }

    @Test
    public void shouldSplitTwoTrees() {

        // Given
        final TreeSplitter splitter = new TreeSplitter();
        final Node<Integer> firstAdminAreaTree = new Node<>(1, 0)
                .withChild(new Node<>(11, 1)
                        .withChild(new Node<>(111, 2)))
                .withChild(new Node<>(12, 1)
                        .withChild(new Node<>(121, 2))
                        .withChild(new Node<>(122, 2)));
        final Node<Integer> secondAdminAreaTree = new Node<>(3, 0)
                .withChild(new Node<>(31, 1)
                        .withChild(new Node<>(311, 2)))
                .withChild(new Node<>(32, 1)
                        .withChild(new Node<>(321, 2))
                        .withChild(new Node<>(322, 2)));
        final Node<Integer> geopoliticalTree = new Node<>(2, 0)
                .withChild(new Node<>(21, 1)
                        .withChild(new Node<>(111, 2)))
                .withChild(new Node<>(22, 1)
                        .withChild(new Node<>(322, 2)));
        // When
        final Collection<Node<Integer>> splited =
            splitter.split(Arrays.asList(firstAdminAreaTree, secondAdminAreaTree), Collections.singleton(geopoliticalTree));
        // Then
        final Node<Integer> actualTrees = new Node<>(0, 0).withChildren(splited);
        final Node<Integer> expectedTrees = new Node<>(0, 0)
                .withChild(new Node<>(1, 0)
                        .withChild(new Node<>(12, 1)
                                .withChild(new Node<>(121, 2))
                                .withChild(new Node<>(122, 2))))
                .withChild(new Node<>(2, 0)
                        .withChild(new Node<>(11, 1)
                                .withChild(new Node<>(111, 2)))
                        .withChild(new Node<>(22, 1)
                                .withChild(new Node<>(322, 2))))
                .withChild(new Node<>(3, 0)
                        .withChild(new Node<>(31, 1)
                                .withChild(new Node<>(311, 2)))
                        .withChild(new Node<>(32, 1)
                                .withChild(new Node<>(321, 2))));
        assertThat("Actual and expected trees are different!", actualTrees, is(expectedTrees));
    }

    @Test
    public void shouldHandleComplexTree() {

        // Given
        /*
         * level0 : 1,                                                                    2
         * level1 : 11,                                       12,                         21
         * level8 : 111,                        112,          121,          122,          211,   212
         * level9 : 1111,         1112,                                                   2111
         * leaves : 11111, 11112, 11121, 11122, 11201, 11202, 12101, 12102, 12201, 12202, 11112, 11201, 11202
         */
        final TreeSplitter splitter = new TreeSplitter();
        final Node<Integer> adminAreaTree = new Node<>(1, 0)
                .withChild(new Node<>(11, 1)
                        .withChild(new Node<>(111, 2)
                                .withChild(new Node<>(1111, 3)
                                        .withChild(new Node<>(11111, 0))
                                        .withChild(new Node<>(11112, 0)))
                                .withChild(new Node<>(1112, 3)
                                        .withChild(new Node<>(11121, 0))
                                        .withChild(new Node<>(11122, 0))))
                        .withChild(new Node<>(112, 2)
                                .withChild(new Node<>(11201, 0))
                                .withChild(new Node<>(11202, 0))))
                .withChild(new Node<>(12, 1)
                        .withChild(new Node<>(121, 2)
                                .withChild(new Node<>(12101, 0))
                                .withChild(new Node<>(12102, 0)))
                        .withChild(new Node<>(122, 2)
                                .withChild(new Node<>(12201, 0))
                                .withChild(new Node<>(12202, 0))));
        final Node<Integer> geopoliticalTree = new Node<>(2, 0)
                .withChild(new Node<>(21, 1)
                        .withChild(new Node<>(211, 2)
                                .withChild(new Node<>(2111, 3)
                                        .withChild(new Node<>(11112, 0))))
                        .withChild(new Node<>(212, 2)
                                .withChild(new Node<>(11201, 0))
                                .withChild(new Node<>(11202, 0))));
        // When
        final Collection<Node<Integer>> splited =
            splitter.split(Collections.singleton(adminAreaTree), Collections.singleton(geopoliticalTree));
        // Then
        final Node<Integer> actualTrees = new Node<>(0, 0).withChildren(splited);
        final Node<Integer> expectedTrees = new Node<>(0, 0)
                .withChild(new Node<>(1, 0)
                        .withChild(new Node<>(11, 0)
                                .withChild(new Node<>(111, 0)
                                        .withChild(new Node<>(1111, 0)
                                                .withChild(new Node<>(11111, 0)))
                                        .withChild(new Node<>(1112, 0)
                                                .withChild(new Node<>(11121, 0))
                                                .withChild(new Node<>(11122, 0)))))
                        .withChild(new Node<>(12, 0)
                                .withChild(new Node<>(121, 0)
                                        .withChild(new Node<>(12101, 0))
                                        .withChild(new Node<>(12102, 0)))
                                .withChild(new Node<>(122, 0)
                                        .withChild(new Node<>(12201, 0))
                                        .withChild(new Node<>(12202, 0)))))
                .withChild(new Node<>(2, 0)
                        .withChild(new Node<>(21, 0)
                                .withChild(new Node<>(211, 0)
                                        .withChild(new Node<>(2111, 0)
                                                .withChild(new Node<>(11112, 0))))
                                .withChild(new Node<>(112, 0)
                                        .withChild(new Node<>(11201, 0))
                                        .withChild(new Node<>(11202, 0)))));
        assertThat("Actual and expected trees are different!", actualTrees, is(expectedTrees));
    }
}
